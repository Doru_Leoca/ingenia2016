clear
clc
close all

x=0:255;
y=x;
z=x;
%cuando el valor es muy cercano a los l�mites,
%no es lo mismo estar a un lado que a otro de dicho valor
%(a pesar de estar a la misma distancia de dicho valor)
value=0;
if value==0
    value=10;
end
for i=x+1
    if y(1,i)>value
        prop=(255-value)/(y(1,i)-value);
        z(1,i) = y(1,i) - (255-value)/(prop) - (value)/(prop);
    end
    z(1,i) = z(1,i)/value;
end
plot(x,z);
grid on
xlim([0 255])