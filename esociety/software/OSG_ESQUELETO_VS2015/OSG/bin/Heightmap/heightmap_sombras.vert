//GLSL programming

uniform sampler2D texUnit0;	
uniform sampler2D texUnit1;


varying float height;
varying vec3 normal;

varying vec4 shadowCoords;

void main(void)
{
	vec4 color = texture2D(texUnit0, gl_MultiTexCoord0.st);
	normal = texture2D(texUnit1, gl_MultiTexCoord0.st).xyz;
	normal = normal*2 - vec3(1,1,1);
	normal = gl_NormalMatrix * normal;
	vec4 vertice = gl_Vertex;
	height = color.r*100;
	vertice[2] = height;
	gl_TexCoord[0] = gl_MultiTexCoord0; 
	gl_Position = gl_ModelViewProjectionMatrix * vertice;
	
	vec4 posAux = gl_ModelViewMatrix * vertice; //en coordenadas del punto de vista
	shadowCoords.s = dot(posAux,gl_EyePlaneS[6]);
	shadowCoords.t = dot(posAux,gl_EyePlaneT[6]);
	shadowCoords.p = dot(posAux,gl_EyePlaneR[6]);
	shadowCoords.q = dot(posAux,gl_EyePlaneQ[6]);
}

 
