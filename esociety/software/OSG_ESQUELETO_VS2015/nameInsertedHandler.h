#pragma once

#include <osgGA/TrackballManipulator>
#include <osgWidget/Input>

struct NameInsertedHandler : public osgGA::GUIEventHandler
{

	NameInsertedHandler(int key, osgWidget::Input* in, std::string &n, bool& p/*, SnapImage* si*/) :
		_key(key),
		_input(in),
		_name(n),
		_pressed(p)
	{
		_pressed = false;
	}

	bool handle(const osgGA::GUIEventAdapter& ea, osgGA::GUIActionAdapter&)
	{
		if (ea.getHandled()) return false;

		switch (ea.getEventType())
		{
		case(osgGA::GUIEventAdapter::KEYUP):
		{
			if (ea.getKey() == _key)
			{
				_name = (_input->getText())->getText().createUTF8EncodedString();
				printf("Name inserted: %s\n", _name.c_str());
				_pressed = true;
				return true;
			}

			break;
		}
		default:
			break;
		}

		return false;
	}

	int                     _key;
	osgWidget::Input*		_input;
	std::string&			_name;
	bool&					_pressed;
};