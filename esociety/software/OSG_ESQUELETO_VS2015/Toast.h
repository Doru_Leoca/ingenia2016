#pragma once

#include <string>
#include <list>
#include "atltime.h"

struct Data {
	std::string message;
	unsigned int time;
	unsigned int t0;
	
	Data::Data(std::string s, unsigned int t);
};

struct DataWithKey {
	std::string message;
	int key;
	
	DataWithKey::DataWithKey(std::string s, int k);
};

class Toast
{
private:
	std::list<Data> listD;
	std::list<DataWithKey> listDWK;
	std::list<int> *listKeys;
public:
	Toast();
	void setKeyList(std::list<int> *k);
	void addMessage(std::string s, unsigned int t=3);
	void addMessageWithKey(std::string s, unsigned char t=3);
	std::string getMessages();
	~Toast();
};

