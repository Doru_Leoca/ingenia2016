
#include "Guardar.h"

int Guardar(ArchivosGuardar datos, std::string nombre) {
	XmlWriter xml;
	std::list<Objeto> listaYo;
	std::list<Objeto> listaJack;
	std::list<Objeto> listaMax;
	std::string s;

	listaYo = datos.listaYo;
	//
	std::string nombre_archivo = "./PartidasGuardadas/" + datos.NOMBRE + ".xml";
	if (xml.open(nombre_archivo)) {

		xml.writeOpenTag(nombre);
		xml.writeOpenTag("posicion");

		xml.writeStartElementTag("posX");
		std::string aux = std::to_string(datos.posX);
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();

		xml.writeStartElementTag("posY");
		aux = std::to_string(datos.posY);
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();

		xml.writeStartElementTag("posZ");
		aux = std::to_string(datos.posZ);
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();
		
		xml.writeStartElementTag("dir");
		aux = std::to_string(datos.dir);
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();

		xml.writeCloseTag();

		//objetos
		std::list<Objeto>::iterator it = listaYo.begin();
		xml.writeOpenTag("listaYo");
		for (it; it != listaYo.end(); it++) {
			xml.writeOpenTag("objeto");

			xml.writeStartElementTag("Nombre");
			xml.writeString(it->getName().c_str());
			xml.writeEndElementTag();


			s = std::to_string(it->getX());

			xml.writeStartElementTag("posX");
			xml.writeString(s.c_str());
			xml.writeEndElementTag();

			s = std::to_string(it->getY());

			xml.writeStartElementTag("posY");
			xml.writeString(s.c_str());
			xml.writeEndElementTag();

			s = std::to_string(it->getZ());

			xml.writeStartElementTag("posZ");
			xml.writeString(s.c_str());
			xml.writeEndElementTag();

			s = std::to_string(it->getQuantity());

			xml.writeStartElementTag("cantidad");
			xml.writeString(s.c_str());
			xml.writeEndElementTag();

			xml.writeCloseTag();
		}
		xml.writeCloseTag();



		//camara
		xml.writeOpenTag("RestoVariables");
		xml.writeStartElementTag("Scroll_times");
		aux = std::to_string(datos.scroll_times);
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();

		xml.writeStartElementTag("anguloCamaraVertical0");
		aux = std::to_string(datos.anguloCamaraVertical0);
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();

		xml.writeStartElementTag("anguloCamaraHorizontal0");
		aux = std::to_string(datos.anguloCamaraHorizontal0);
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();

		xml.writeStartElementTag("anguloCamaraVertical_1aPersona");
		aux = std::to_string(datos.anguloCamaraVertical_1aPersona);
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();

		xml.writeStartElementTag("anguloCamaraHorizontal_1aPersona");
		aux = std::to_string(datos.anguloCamaraHorizontal_1aPersona);
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();


		//juego

		xml.writeStartElementTag("FASE_JUEGO");
		aux = datos.FASE_JUEGO;
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();

		xml.writeStartElementTag("FASE_JUEGOprev");
		aux = datos.FASE_JUEGOprev;
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();

		xml.writeStartElementTag("NOMBRE");
		aux = datos.NOMBRE;
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();

		xml.writeStartElementTag("tiempoCompletado");
		aux = std::to_string(datos.tiempoCompletado);
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();

		//sol

		xml.writeStartElementTag("thetaSol");
		aux = std::to_string(datos.thetaSol);
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();

		xml.writeStartElementTag("phiSol");
		aux = std::to_string(datos.phiSol);
		xml.writeString(aux.c_str());
		xml.writeEndElementTag();

		xml.writeCloseTag();

		xml.writeCloseTag();
		xml.close();
		std::cout << "Success!\n";
	}
	else {
		std::cout << "Error opening file.\n";
	}

	return 0;
}