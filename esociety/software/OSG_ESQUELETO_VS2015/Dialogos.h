#pragma once

//#define MOSTRAR_DIALOGOS 1

#include <string>
#include "rapidxml-1.13/rapidxml.hpp"
#include <osgDB/ReadFile>
#include "MACRO_MOSTRAR.h"

struct Pregunta {
	std::string pregunta;
	std::string da;
	int conrespuesta;
};

struct Respuesta {
	std::string respuesta;
	std::string siguientefase;
	std::string necesario;
	std::string sino;
};

struct Dialogo {
	std::string fase;
	std::string personaje;
	Pregunta pregunta;
	Respuesta respuesta[3];
};

class GuardaDialogos {
public:
	std::string nombre;
	Dialogo* dialogos;
	int numDialogos = 0;

	GuardaDialogos(std::string name) : nombre(name) {};
	void encontrarDialogos();
};