#pragma once

//para los objetos
#define OBJETO_MAPA "./Mapas/mapa3.png"
#define OBJETO_PAQUETE "./Texturas/Objetos/paquete.jpg"
#define OBJETO_LISTA "./Texturas/Objetos/lista.png"
#define OBJETO_PIEZAS "./Texturas/Objetos/piezas_motor.jpg"
#define OBJETO_PISTOLA "./Texturas/Objetos/pistola.jpg"
#define OBJETO_BALA "./Texturas/Objetos/bala.jpg"
#define OBJETO_LAPIZ "./Texturas/Objetos/lapiz.png"
#define OBJETO_3PUNTOS "./Texturas/Objetos/three_dots.png"
#define OBJETO_BLANCO "./Texturas/Objetos/blanco.png"
