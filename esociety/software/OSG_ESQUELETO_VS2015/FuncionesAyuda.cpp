#include "stdafx.h"	
#include "FuncionesAyuda.h"

float obtenerAltura(float posX, float posY,osg::Image* imagen_heightmap, int num_puntos,double separacion_puntos,double altura_total)
{
	float tamMalla = num_puntos*separacion_puntos; 
	
	float xGeom = (posX + (tamMalla/2))/(tamMalla-1);
	float yGeom = (posY + (tamMalla/2))/(tamMalla-1);
	//printf("x=%f,y=%f\n", x1, x2, y1, y2);
	int x1 = (posX + (tamMalla / 2));
	if (x1 < 0)
		x1 -= 1;
	int x2 = x1 + 1;	
	
	int y1 = (posY + (tamMalla / 2));
	if (y1 < 0)
		y1 -= 1;
	int y2 = y1 + 1;
	//printf("x1=%d, x2=%d, y1=%d, y2=%d\t", x1, x2, y1, y2);
	//printf("tm=%f\t", tamMalla);
	float x1bis = x1/(tamMalla - 1);
	float x2bis = x2/(tamMalla - 1);
	float y1bis = y1/(tamMalla - 1);
	float y2bis = y2/(tamMalla - 1);
	//printf("x1=%f, x2=%f, y1=%f, y2=%f\n", x1bis, x2bis, y1bis, y2bis);

	double z1 = imagen_heightmap->getColor(osg::Vec2(x1bis, y1bis)).r()*altura_total;
	double z2 = imagen_heightmap->getColor(osg::Vec2(x2bis, y1bis)).r()*altura_total;
	double z3 = imagen_heightmap->getColor(osg::Vec2(x2bis, y2bis)).r()*altura_total;
	double z4 = imagen_heightmap->getColor(osg::Vec2(x1bis, y2bis)).r()*altura_total;
	
	//printf("z1=%f, z2=%f, z3=%f, z4=%f\n", z1, z2, z3, z4);

	//Triangulo en que se encuentra el punto
	//primera opcion
	float v_k12 = (x1bis - xGeom)*(y1bis - yGeom) - (x2bis - xGeom)*(y1bis - yGeom);
	float v_k23 = (x2bis - xGeom)*(y2bis - yGeom) - (x2bis - xGeom)*(y1bis - yGeom);
	float v_k31 = (x2bis - xGeom)*(y1bis - yGeom) - (x1bis - xGeom)*(y2bis - yGeom);
	

	// segunda opcion//
	/*float v_k12 = (x1bis - xGeom)*(y1bis - yGeom) - (x2bis - xGeom)*(y1bis - yGeom);
	float v_k24 = (x2bis - xGeom)*(y2bis - yGeom) - (x1bis - xGeom)*(y1bis - yGeom);
	float v_k41 = (x1bis - xGeom)*(y1bis - yGeom) - (x1bis - xGeom)*(y2bis - yGeom);
	*/
	float posZ = 0;

	//Primera opcion
	if (v_k12>0 && v_k23>0 && v_k31>0) {
		posZ = z1 + (z2 - z1)*(xGeom - x1bis) / (x2bis - x1bis) + (z3 - z2)*(yGeom - y1bis) / (y2bis - y1bis);
	}
	else {
		posZ = z1 + (z4 - z1)*(yGeom - y1bis) / (y2bis - y1bis) + (z3 - z4)*(xGeom - x1bis) / (x2bis - x1bis);
	}
	//printf("posZ=%f\n", posZ);
	

	//segunda opcion
	/*if (v_k12>0 && v_k24>0 && v_k41>0) {
		posZ = z4 + (z1 - z4)*(yGeom - y2bis) / (y1bis - y2bis) + (z2 - z1)*(xGeom - x1bis) / (x2bis - x1bis);
	}
	else {
		posZ = z4 + (z2 - z2)*(yGeom - y2bis) / (y1bis - y2bis) + (z3 - z4)*(xGeom - x1bis) / (x2bis - x1bis);
	}*/
	//printf("posZ=%f\n", posZ);

	return posZ;
}


osg::Geode* CreaMalla(int num_puntos,double separacion_puntos)
{
	osg::Geometry* geom = new osg::Geometry;
	osg::Geode* geode = new osg::Geode;
	geode->addDrawable(geom);
	
	osg::Vec3Array* v = new osg::Vec3Array;
	osg::Vec2Array* texcoords1 = new osg::Vec2Array(); //Vectores de coordenadas
	osg::Vec2Array* texcoords2 = new osg::Vec2Array();
	for( int y = -num_puntos/2; y < num_puntos/2; y++)
	{
		for( int x = -num_puntos/2; x < num_puntos/2; x++)
		{
			double a = double(x*separacion_puntos);
			double b =  double(y*separacion_puntos);
			v->push_back(osg::Vec3(a,b,0)); //Push_back es como la funci�n que a�ad�a al final de una lista
			texcoords1->push_back(osg::Vec2(((float)(x+(num_puntos/2))/(num_puntos-1)),((float)(y+(num_puntos/2)))/(num_puntos-1))); //Algoritmo para traducir a coordenadas
			texcoords2->push_back(osg::Vec2(((float)(x+(num_puntos/2))/(num_puntos-1)),((float)(y+(num_puntos/2)))/(num_puntos-1)));
		}
	}
	

	geom->setVertexArray(v);
	geom->setTexCoordArray(0,texcoords1);
	geom->setTexCoordArray(1,texcoords2);


	osg::DrawElementsUInt* elements = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS);
	for(unsigned int y=0;y<num_puntos-1;y++)
	{
		for(unsigned int x=0;x<num_puntos-1;x++)
		{
			elements->push_back((y+1)*(num_puntos)+x);
			elements->push_back(y*(num_puntos)+x);
			elements->push_back(y*(num_puntos)+x+1);
			elements->push_back((y+1)*(num_puntos)+x+1);
		}
	}
	
	
	geom->addPrimitiveSet(elements);
	return geode;
}

void AddTexture(osg::Node* node, std::string nombre_imagen, int unidad_textura)
{
	const char *vinit[] = {"texUnit0","texUnit1","texUnit2","texUnit3","texUnit4","texUnit5","texUnit6","texUnit7"};
	osg::Uniform*	t = new osg::Uniform(vinit[unidad_textura],unidad_textura); //Creaci�n de uniforms, cuidado de no pisar estos al hacer uno manual


	node->getOrCreateStateSet()->addUniform(t);

	
	
	osg::Image* image = osgDB::readImageFile( nombre_imagen);	//Lectura imagen
	osg::Texture2D* textura = new osg::Texture2D(image);
	textura->setFilter(osg::Texture2D::MIN_FILTER,osg::Texture2D::LINEAR_MIPMAP_LINEAR);
	textura->setFilter(osg::Texture2D::MAG_FILTER,osg::Texture2D::LINEAR);
	textura->setWrap(osg::Texture2D::WRAP_R, osg::Texture2D::CLAMP_TO_EDGE);
	textura->setWrap(osg::Texture2D::WRAP_S,osg::Texture2D::REPEAT); //Esto era CLAMP, temas de bordes, repasar llegado el momento, creo que por eso vuelve a subir fuera del heightmap
	textura->setWrap(osg::Texture2D::WRAP_T,osg::Texture2D::REPEAT);

	
	node->getOrCreateStateSet()->setTextureAttributeAndModes(unidad_textura,textura,osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE | osg::StateAttribute::PROTECTED);
	
	osg::AlphaFunc* alphaFunc = new osg::AlphaFunc;
    alphaFunc->setFunction(osg::AlphaFunc::GEQUAL,0.05f);
	node->getOrCreateStateSet()->setAttributeAndModes( alphaFunc, osg::StateAttribute::ON );

}

void AddShader(osg::Node* node,std::string vertex_shader,std::string fragment_shader)
{
	osg::Program* ProgramObject = new osg::Program;

	osg::Shader* VertexObject =   new osg::Shader( osg::Shader::VERTEX );
	VertexObject->loadShaderSourceFromFile(vertex_shader);

	osg::Shader* FragmentObject = new osg::Shader( osg::Shader::FRAGMENT);
	FragmentObject->loadShaderSourceFromFile(fragment_shader);

	ProgramObject->addShader(FragmentObject) && ProgramObject->addShader(VertexObject);

	node->getOrCreateStateSet()->setAttributeAndModes(ProgramObject, osg::StateAttribute::ON | osg::StateAttribute::PROTECTED);

}
