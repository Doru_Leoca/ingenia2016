#include "Dialogos.h"

void GuardaDialogos::encontrarDialogos() {
	rapidxml::xml_document<> docD;
	// Read the xml file into a vector
	std::string nombre_archivo = "./Archivos/dialogos" + nombre + ".xml";
	osgDB::ifstream theFileD(nombre_archivo.c_str());
	std::vector<char> bufferD((std::istreambuf_iterator<char>(theFileD)), std::istreambuf_iterator<char>());
	bufferD.push_back('\0');
	// Parse the buffer using the xml file parsing library into doc }
	docD.parse<0>(&bufferD[0]);
	rapidxml::xml_node<> *nodoD0;
	nodoD0 = docD.first_node();

	PRINT_DIALOGOS("\n\n");
	int i = 0;
	for (rapidxml::xml_node<>* nodoD1 = nodoD0->first_node("dialogo"); nodoD1; nodoD1 = nodoD1->next_sibling("dialogo")) {
		std::string personaje = nodoD1->first_node("personaje")->value();
		PRINT_DIALOGOS("%s\n", personaje.c_str());
		if (personaje == nombre)
			i++;
	}
	numDialogos = i;
	PRINT_DIALOGOS("Dialogos de %s:\n", nombre.c_str());
	PRINT_DIALOGOS("numDialogos: %d\n", numDialogos);
	dialogos = new Dialogo[numDialogos];
	i = 0;
	for (rapidxml::xml_node<>* nodoD1 = nodoD0->first_node("dialogo"); nodoD1; nodoD1 = nodoD1->next_sibling("dialogo")) {
		std::string fase = nodoD1->first_node("fase")->value();
		std::string personaje = nodoD1->first_node("personaje")->value();
		if (personaje == nombre) {
			dialogos[i].fase = fase;
			dialogos[i].personaje = personaje;
			PRINT_DIALOGOS("%s\n", fase.c_str());
			PRINT_DIALOGOS("%s\n", personaje.c_str());

			rapidxml::xml_node<>* nodoP = nodoD1->first_node("pregunta");
			std::string pregunta = nodoP->value();
			std::string da = nodoP->first_attribute()->value();
			std::string conrespuesta = nodoP->first_attribute()->next_attribute()->value();
			dialogos[i].pregunta.pregunta = pregunta;
			dialogos[i].pregunta.da = da;
			dialogos[i].pregunta.conrespuesta = atoi(conrespuesta.c_str());
			PRINT_DIALOGOS("%s\n", pregunta.c_str());
			PRINT_DIALOGOS("%s\n", da.c_str());
			PRINT_DIALOGOS("%s\n", conrespuesta.c_str());

			rapidxml::xml_node<>* nodoR;
			std::string respuesta, siguientefase, necesario, sino;

			for (int j = 0; j < 3; j++) {
				std::string s = "respuesta" + std::to_string(j + 1);
				nodoR = nodoD1->first_node(s.c_str());
				respuesta = nodoR->value();
				siguientefase = nodoR->first_attribute()->value();
				necesario = nodoR->first_attribute()->next_attribute()->value();
				sino = nodoR->first_attribute()->next_attribute()->next_attribute()->value();
				dialogos[i].respuesta[j].respuesta = respuesta;
				dialogos[i].respuesta[j].siguientefase = siguientefase;
				dialogos[i].respuesta[j].necesario = necesario;
				dialogos[i].respuesta[j].sino = sino;
				PRINT_DIALOGOS("%s\n", respuesta.c_str());
				PRINT_DIALOGOS("%s\n", siguientefase.c_str());
				PRINT_DIALOGOS("%s\n", necesario.c_str());
				PRINT_DIALOGOS("%s\n", sino.c_str());
			}
			PRINT_DIALOGOS("\n");
			i++;
		}
	}
}
