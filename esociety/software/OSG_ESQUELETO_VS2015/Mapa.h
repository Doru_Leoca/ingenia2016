#pragma once

#include <osg/Image>
#include "colores.h"
//#include "constantes.h"

class Mapa
{
	private:
		osg::ref_ptr<osg::Image> imagen;
		osg::ref_ptr<osg::Image> imagenOriginal;
		osg::ref_ptr<osg::Image> imagenCursor;
		std::list<osg::Vec2> pixelesModificados;
	public:
		Mapa();
		~Mapa();
		void setImagen(osg::ref_ptr<osg::Image> im);
		void setImagenOriginal(osg::ref_ptr<osg::Image> im);
		void setImagenCursor(osg::ref_ptr<osg::Image> im);
		osg::ref_ptr<osg::Image> getImagen();
		void dibujarFlecha(double x, double y, double dir);
		void desdibujarFlecha(float x, float y);
};

