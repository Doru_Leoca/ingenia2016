#include "stdafx.h"
#include "AudioItemMusica.h"


AudioItemMusica::AudioItemMusica()
{
	error = 0;
	PRINT_SONIDO("musica creada (constructor vacio)\n");
}

AudioItemMusica::AudioItemMusica(std::string p)
{
	error = 0;
	path = p;
	music = Mix_LoadMUS(path.c_str());
	if (music == NULL) {
		error = 1;
		PRINT_SONIDO("ERROR al crear musica: %s\n", path.c_str());
	}
	else
		PRINT_SONIDO("musica creada: %s\n", path.c_str());
}


AudioItemMusica::~AudioItemMusica()
{
	Mix_FreeMusic(music);
}

void AudioItemMusica::setPath(std::string str) {
	path = str;
	music = Mix_LoadMUS(path.c_str());
	if (music == NULL)
		error = 1;
	else
		PRINT_SONIDO("music path modificado: %s\n", path.c_str());
}

void AudioItemMusica::play(int veces)
{
	if (veces >= 0) //si veces es positivo hay que restarle uno para que el sonido se repita (veces-1) veces
		veces--;
	else
		veces = -1; //si veces es negativo o cero se repetir� infinitas veces
	if( Mix_PlayMusic(music, veces)==-1 ) //�ltimo argumento: veces que se repite (-1=infinitas veces)
		error = 2;
}

