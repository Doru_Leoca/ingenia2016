#include "Toast.h"

Data::Data(std::string s, unsigned int t) {
	message = s;
	time = t;
	t0 = ::GetCurrentTime();
}

DataWithKey::DataWithKey(std::string s, int k) {
	message = s;
	key = k;
}

Toast::Toast()
{
}

void Toast::setKeyList(std::list<int>* k)
{
	listKeys = k;
}

void Toast::addMessage(std::string s, unsigned int t)
{
	Data d(s, t);
	listD.push_back(d);
}

void Toast::addMessageWithKey(std::string s, unsigned char t)
{
	DataWithKey d(s, t);
	listDWK.push_back(d);
}

std::string Toast::getMessages()
{
	std::string s = "";
	for (std::list<Data>::iterator it = listD.begin(); it != listD.end(); it++) {
		unsigned int t1 = ::GetCurrentTime();
		unsigned int t0 = it->t0;
		unsigned int T = it->time;
		if ((t1 - t0) < (T * 1000)) {
			s += it->message;
			s += "\n";
		}
		else
			listD.erase(it);
	}
	for (std::list<DataWithKey>::iterator it = listDWK.begin(); it != listDWK.end(); it++) {
		bool teclaPulsada = std::find(listKeys->begin(), listKeys->end(), it->key) != listKeys->end();
		if (!teclaPulsada) {
			s += it->message;
			s += "\n";
		}
		else
			listDWK.erase(it);
	}
	return s;
}


Toast::~Toast()
{
}
