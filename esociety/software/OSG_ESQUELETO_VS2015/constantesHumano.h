#pragma once

//para el humano
#define PENDIENTE_MAX 1.0
#define YO_X0 -190.0
#define YO_Y0 -70.0
#define YO_Z0 0 //esta no importa ya que se recalculará
#define YO_DIRECCION0 -90.0
#define YO_X1 -169.0 //X1 está siendo usado por osg
#define YO_Y1 158.0
#define YO_Z1 0 //esta no importa ya que se recalculará
#define YO_DIRECCION1 80.0

#define MAX_X0 -168.0
#define MAX_Y0 160.0
#define MAX_Z0 0 //esta no importa ya que se recalculará
#define MAX_DIRECCION0 -90.0

#define JACK_X0 188.0
#define JACK_Y0 204.6
#define JACK_Z0 0 //esta no importa ya que se recalculará
#define JACK_DIRECCION0 -180.0

#define BARMAN_X0 185.6
#define BARMAN_Y0 208.3
#define BARMAN_Z0 0 //esta no importa ya que se recalculará
#define BARMAN_DIRECCION0 -90.0

#define JEFE_X0 -60.0
#define JEFE_Y0 -86.0
#define JEFE_Z0 0 //esta no importa ya que se recalculará
#define JEFE_DIRECCION0 0.0

#define GUARDIA_X0 6.0
#define GUARDIA_Y0 -84.0
#define GUARDIA_Z0 0 //esta no importa ya que se recalculará
#define GUARDIA_DIRECCION0 -135.0

#define CTE_GIRO 0.05
#define CTE_AVANCE 0.2
#define VEL_ANDAR 5
#define DESNIVEL 0.1
#define DISTANCIA_MIN_A_OTROS 3.0
