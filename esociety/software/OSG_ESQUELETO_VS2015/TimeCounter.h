#pragma once


class TimeCounter
{
	unsigned int t1, t2;
public:
	TimeCounter();
	~TimeCounter();

	void takeTime();
	void printTime(); //imprime el tiempo entre takeTime() y printTime()
};

