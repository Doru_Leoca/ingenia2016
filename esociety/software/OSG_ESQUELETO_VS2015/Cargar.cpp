
#include "stdafx.h"
#include <iostream>
//#include <fstream>
#include <string>
#include "ArchivosGuardar.h"
#include <stdio.h>
#include "Cargar.h"
#include "osgdb/fstream"

#include "rapidxml-1.13/rapidxml.hpp"

char* file_to_char_array(std::string file_name) {
	int num_characters = 0;
	osgDB::ifstream myfile(file_name.c_str());

	if (myfile.is_open())
	{
		char c;
		while (myfile.get(c))
		{
			num_characters++;
			//std::cout << c;
		}
		myfile.clear();
		myfile.seekg(0, myfile.beg); //para leer el archivo otra vez desde el principio
		char* array = new char[num_characters + 1];
		int i = 0;
		while (myfile.get(c))
		{
			array[i] = c;
			//std::cout << c;
			i++;
		}
		array[i] = '\0';
		myfile.close();
		return array;
	}
	else std::cout << "Unable to open file";

	return NULL;
}

ArchivosGuardar Cargar(std::string nombre)
{
	ArchivosGuardar Carga;
	unsigned int cantidad;

	std::string nombre_archivo = "./PartidasGuardadas/" + nombre + ".xml";
	//pasa el archivo de texto a un array de caracteres
	char* a = file_to_char_array(nombre_archivo);

	//imprime la cadena obtenida
	std::cout << "Cadena obtenida:\n\n";
	int i = 0;
	while (a[i] != '\0') {
		std::cout << a[i];
		i++;
	}
	std::cout << std::endl << std::endl;

	{
		using namespace rapidxml;
		xml_document<> doc;    // character type defaults to char
		doc.parse<0>(a);    // 0 means default parse flags
		xml_node<> *nodo0 = doc.first_node();
		std::cout << "nodo padre (" << nodo0->name() << ") tiene:\n";
		int i = 1;
		xml_node<> *nodo1 = nodo0->first_node("posicion");  

		//posicionPersonaje
			Carga.posX= strtod(nodo1->first_node("posX")->value(),NULL);
			Carga.posY= strtod(nodo1->first_node("posY")->value(),NULL);
			Carga.posZ= strtod(nodo1->first_node("posZ")->value(),NULL);
			Carga.dir= strtod(nodo1->first_node("dir")->value(),NULL);

		//listaYo
			nodo1 = nodo0->first_node("listaYo");
			xml_node<> *nodo2 = nodo1->first_node();
			while (nodo2 != NULL) {
				xml_node<> *nodo3 = nodo2->first_node();
				nombre = nodo3->value();
				nodo3 = nodo3->next_sibling();

				double xaux = strtod(nodo3->value(), NULL);;
				nodo3 = nodo3->next_sibling();

				double yaux = strtod(nodo3->value(), NULL);;
				nodo3 = nodo3->next_sibling();

				double zaux = strtod(nodo3->value(), NULL);;
				nodo3 = nodo3->next_sibling();

				cantidad = (unsigned int)strtod(nodo3->value(), NULL);

				Carga.listaYo.push_back(Objeto(nombre, Punto(xaux, yaux, zaux), cantidad));

				nodo2 = nodo2->next_sibling();
			}
			
		//RestoVariables
			nodo1 = nodo0->first_node("RestoVariables");
			
			Carga.scroll_times = (int)strtod(nodo1->first_node("Scroll_times")->value(), NULL);
			Carga.anguloCamaraVertical0 = (int)strtod(nodo1->first_node("anguloCamaraVertical0")->value(), NULL);
			Carga.anguloCamaraHorizontal0 = (int)strtod(nodo1->first_node("anguloCamaraHorizontal0")->value(), NULL);
			Carga.anguloCamaraVertical_1aPersona = (int)strtod(nodo1->first_node("anguloCamaraVertical_1aPersona")->value(), NULL);
			Carga.anguloCamaraHorizontal_1aPersona = (int)strtod(nodo1->first_node("anguloCamaraHorizontal_1aPersona")->value(), NULL);
			Carga.FASE_JUEGO = nodo1->first_node("FASE_JUEGO")->value();
			Carga.FASE_JUEGOprev = nodo1->first_node("FASE_JUEGOprev")->value();
			Carga.NOMBRE = nodo1->first_node("NOMBRE")->value();
			Carga.tiempoCompletado = (int)strtod(nodo1->first_node("tiempoCompletado")->value(), NULL);
			Carga.thetaSol = (int)strtod(nodo1->first_node("thetaSol")->value(), NULL);
			Carga.phiSol = (int)strtod(nodo1->first_node("phiSol")->value(), NULL);
		
	}

	return Carga;
}
