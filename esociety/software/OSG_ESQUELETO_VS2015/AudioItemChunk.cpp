#include "stdafx.h"
#include "AudioItemChunk.h"


AudioItemChunk::AudioItemChunk()
{
	error = 0;
	printf("chunk creado (constructor vacio)\n");
}

AudioItemChunk::AudioItemChunk(std::string p)
{
	error = 0;
	path = p;
	wave = Mix_LoadWAV(path.c_str());
	if (wave == NULL) {
		error = 1;
		PRINT_SONIDO("ERROR al crear musica: %s\n", path.c_str());
	}
	else
		PRINT_SONIDO("chunk creado: %s\n", path.c_str());
}


AudioItemChunk::~AudioItemChunk()
{
	Mix_FreeChunk(wave);
}

void AudioItemChunk::setPath(std::string str) {
	path = str;
	wave = Mix_LoadWAV(path.c_str());
	if (wave == NULL)
		error = 1;
	else
		PRINT_SONIDO("chunk path modificado: %s\n", path.c_str());
}

void AudioItemChunk::play(int veces)
{
	if (veces >= 0) //si veces es positivo hay que restarle uno para que el sonido se repita (veces-1) veces
		veces--;
	else
		veces = -1; //si veces es negativo o cero se repetir� infinitas veces
	channel = Mix_PlayChannel(-1, wave, veces);
	if (channel == -1) //�ltimo argumento: veces que se repite (-1=infinitas veces)
		error = 2;
}

void AudioItemChunk::pause()
{
	Mix_Pause(channel);
}

void AudioItemChunk::resume()
{
	Mix_Resume(channel);
}

bool AudioItemChunk::isPlaying()
{
	int i = Mix_Playing(channel);
	if (i == 1)
		return true;
	return false;
}

bool AudioItemChunk::isPaused()
{
	int i = Mix_Paused(channel);
	if (i == 1)
		return true;
	return false;
}

int AudioItemChunk::setVolume(int volume)
//de 0 a 128
//si se le pasa un valor negativo, se queda igual
//devuelve el anterior valor del volumen
{
	return Mix_VolumeChunk(wave, volume);
}

