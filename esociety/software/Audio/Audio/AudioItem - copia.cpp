#include "stdafx.h"
#include "AudioItem.h"


AudioItem::AudioItem()
{
	printf("constructor vacio\n");
}

AudioItem::AudioItem(std::string name, std::string p, bool esC)
{
	error = 0;
	path = p;
	esChunk = esC;
	id = name;
	if(esChunk)
		wave = Mix_LoadWAV(path.c_str());
	else
		music = Mix_LoadMUS(path.c_str());
	if (wave == NULL && music == NULL)
		error = 3;
	else
		printf("audio creado\n");
}


AudioItem::~AudioItem()
{
	if(esChunk)
		Mix_FreeChunk(wave);
	else
		Mix_FreeMusic(music);
}

void AudioItem::playAudio(int veces)
{
	if (esChunk)
		/*printf("%d\n", error);*/ Mix_PlayChannel(-1, wave, 0); //primer argumento: canal (-1=canal que est� disponible)		//tercer argumento: veces que se repite (-1=infinitas veces)
	else
		/*printf("music\n");*/Mix_PlayMusic(music, veces); //�ltimo argumento: veces que se repite (-1=infinitas veces)
	//if (canal == -1)
		//error = 4;
}

