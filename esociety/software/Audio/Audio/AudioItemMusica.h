#pragma once

#include <string>
#include "SDL.h"
#include "SDL_mixer.h"

#include <stdio.h>

class AudioItemMusica
{
private:
	unsigned int error;
	std::string path;
	Mix_Music *music = NULL;
public:
	AudioItemMusica();
	AudioItemMusica(std::string p);
	~AudioItemMusica();
	unsigned int getError() { return error; }
	void setPath(std::string str) { path = str; }
	std::string getPath() { return path; }
	void play(int veces);
};

